
drop database airline;
create database airline;
use airline;
-- -----------------------------------------------------
-- Table `airline`.`Destination`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Destination` (
  `destination_id` INT NOT NULL,
  `city` VARCHAR(50) NULL,
  `country` VARCHAR(50) NULL,
  PRIMARY KEY (`destination_id`))
ENGINE = InnoDB;
  
-- -----------------------------------------------------
-- Table `airline`.`Airport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Airport` (
  `airport_id` INT NOT NULL,
  `name` VARCHAR(60) NULL,
  `has_carrental` char(1),
  `has_hotel` char(1),
  `airport_destination_id` INT NOT NULL,
  PRIMARY KEY (`airport_id`),
  INDEX `fk_airport_destination` (`airport_destination_id` ASC),
  CONSTRAINT `fk_airport_destination`
    FOREIGN KEY (`airport_destination_id`)
    REFERENCES `airline`.`Destination` (`destination_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
  
  
 -- -----------------------------------------------------
-- Table `airline`.`Terminal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Terminal` (
  `name` VARCHAR(30) NULL,
  `fk_airport_id` INT NOT NULL,
  INDEX `fk_terminal_airport` (`fk_airport_id` ASC),
  CONSTRAINT `fk_terminal_airport`
    FOREIGN KEY (`fk_airport_id`)
    REFERENCES `airline`.`Airport` (`airport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
  
  
-- -----------------------------------------------------
-- Table `airline`.`Flight`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Flight` (
  `flight_id` INT NOT NULL,
  `to_airport_id` INT NOT NULL,
  `from_airport_id` INT NOT NULL,
  `departure_time` DATETIME NOT NULL,
  `arrival_time` DATETIME NOT NULL,
  `capacity` INT NULL,
  `terminal` varchar(30) NULL,
  `status` VARCHAR(50) NULL DEFAULT 'on time',
  PRIMARY KEY (`flight_id`),
  INDEX `fk_flight_toairport` (`to_airport_id` ASC),
  INDEX `fk_flight_fromairport` (`from_airport_id` ASC),
  CONSTRAINT `fk_flight_toairport`
    FOREIGN KEY (`to_airport_id`)
    REFERENCES `airline`.`Airport` (`airport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_flight_fromairport`
    FOREIGN KEY (`from_airport_id`)
    REFERENCES `airline`.`Airport` (`airport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
  
  
-- -----------------------------------------------------
-- Table `airline`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Customer` (
  `firstname` VARCHAR(50) NOT NULL,
  `lastname` VARCHAR(50) NOT NULL,
  `username` VARCHAR(20) NOT NULL unique,
  `email` VARCHAR(45) NOT NULL,
  `frequent_flyer` char(1),
  `DOB` DATE NOT NULL,
  `marital_status` VARCHAR(45) NULL,
  PRIMARY KEY (`username`))
ENGINE = InnoDB;
  
  
-- -----------------------------------------------------
-- Table `airline`.`Booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Booking` (
  `booking_id` int not null,
  `booked_on` date,
  `checked_in` char(1),
  `paid` DOUBLE NULL,
  `return` char(1),
  `checked_baggage` INT NULL,
  `seat_no` CHAR(5) NULL,
  `priority_boarding` char(1),
  `for_flight_id` INT NOT NULL,
  `Customer_username` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`booking_id`),
  INDEX `fk_booking_flight` (`for_flight_id` ASC),
  INDEX `fk_booking_customer` (`Customer_username` ASC),
  CONSTRAINT `fk_booking_flight`
    FOREIGN KEY (`for_flight_id`)
    REFERENCES `airline`.`Flight` (`flight_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_customer`
    FOREIGN KEY (`Customer_username`)
    REFERENCES `airline`.`Customer` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
  
-- -----------------------------------------------------
-- Table `airline`.`Trip Type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airline`.`Trip Type` (
  `fk_destination_id` INT NOT NULL,
  `seaside` char(1),
  `family` char(1),
  `nightlife` char(1),
  `outdoor` char(1),
  `adventure` char(1),
  `golf` char(1),
  `sport` char(1),
  `cultural` char(1),
  INDEX `fk_type_destination` (`fk_destination_id` ASC),
  CONSTRAINT `fk_type_destination`
    FOREIGN KEY (`fk_destination_id`)
    REFERENCES `airline`.`Destination` (`destination_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;