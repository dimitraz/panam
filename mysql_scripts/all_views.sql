
#drop view customer_bookings;
create view customer_bookings as
select 

	booking_id, DATE_FORMAT(booked_on, '%Y-%m-%d') as 'booked_on', checked_in, paid, `return`, checked_baggage,
    seat_no, priority_boarding,for_flight_id, Customer_username,
    firstname, lastname,
    d1.city as 'from', a1.name as 'from-airport', d2.city as 'to', a2.name as 'to-airport',
    DATE_FORMAT(arrival_time, '%Y-%m-%d %H:%m') as 'arriving', DATE_FORMAT(departure_time, '%Y-%m-%d %H:%m') as 'departing',  terminal as 'terminal'

	from airport a1, airport a2, destination d1, destination d2, booking, flight, customer

	where Customer_username = username
    and for_flight_id = flight_id
    and from_airport_id = a1.airport_id
	and to_airport_id = a2.airport_id
	and a1.airport_destination_id = d1.destination_id
	and a2.airport_destination_id = d2.destination_id;
    
    
#drop view all_flights;
create view all_flights as
select 
	d1.city as 'from', a1.name as 'from-airport', DATE_FORMAT(departure_time, '%Y-%m-%d %H:%m') as 'departing', 
    d2.city as 'to', a2.name as 'to-airport', DATE_FORMAT(arrival_time, '%Y-%m-%d %H:%m') as 'arriving',
	status as 'status',flight_id as 'id',  terminal as 'terminal'

	from airport a1, airport a2, destination d1, destination d2, flight

	where from_airport_id = a1.airport_id
	and to_airport_id = a2.airport_id
	and a1.airport_destination_id = d1.destination_id
	and a2.airport_destination_id = d2.destination_id;
    

  # View to display all flights due to depart in the future
#drop view future_flights;
create view future_flights as
select
    d1.city as 'from', a1.name as 'from-airport', DATE_FORMAT(departure_time, '%Y-%m-%d %H:%m') as 'departing', 
    d2.city as 'to', a2.name as 'to-airport', DATE_FORMAT(arrival_time, '%Y-%m-%d %H:%m') as 'arriving',
    status as 'status', flight_id as 'id',  terminal as 'terminal'
 
    from airport a1, airport a2, destination d1, destination d2, flight
 
    where from_airport_id = a1.airport_id
    and to_airport_id = a2.airport_id
    and a1.airport_destination_id = d1.destination_id
    and a2.airport_destination_id = d2.destination_id
    and departure_time > now();
      
