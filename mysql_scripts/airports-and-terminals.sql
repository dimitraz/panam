
   delimiter $
   create trigger check_airport before insert on airport
   for each row 
		begin 
			if new.has_carrental not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'has_carrental : not Y or N';
			end if;
			if new.has_hotel not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'has_hotel : not Y or N';
			end if;
	    end$
    delimiter ;
        

-- Osaka --
insert into airport values (000101, "Kansai International Airport", 'Y', 'Y', 00010);
insert into terminal values("Terminal 1", 000101);
insert into terminal values("Terminal 2", 000101);
 
insert into airport values (000102, "Itami Airport (Osaka International)", 'Y','N', 00010);
insert into terminal values("Terminal 1", 000102);
 
-- Tokyo --
insert into airport values (000121, "Tokyo Haneda Airport ", 'Y', 'Y', 00012);
insert into terminal values("Terminal 1", 000121);
insert into terminal values("Terminal 2", 000121);
insert into terminal values("International Terminal", 000121);
 
insert into airport values (000122, "Narita International Airport", 'Y', 'Y', 00012);
insert into terminal values("Terminal 1", 000122);
insert into terminal values("Terminal 2", 000122);
insert into terminal values("Terminal 3", 000122);
 
insert into airport values (000123, "Miyakejima Airport", 'N', 'N', 00012);
insert into terminal values("Terminal 1", 000123);
 
insert into airport values (000124, "Oshima Airport", 'N', 'N', 00012);
insert into terminal values("Terminal 1", 000124);
 
insert into airport values (000125, "Hachijojima Airport", 'N', 'N', 00012);
insert into terminal values("Terminal 1", 000125);
 
-- Kyoto: TO DO --
 
-- Seoul -- 
insert into airport values (000201, "Incheon International Airport", 'Y', 'Y', 00020);
insert into terminal values("Main Terminal", 00201);
insert into terminal values("Concourse", 00201);
insert into terminal values("Terminal 2", 000201);
 
insert into airport values (000202, "Gimpo International Airport", 'Y', 'Y', 00020);
insert into terminal values("Domestic Terminal", 000202);
insert into terminal values("International Terminal", 000202);
 
-- Hong Kong --
insert into airport values (000211, "Hong Kong International Airport", 'Y', 'Y', 00021);
insert into terminal values("Terminal 1", 000211);
insert into terminal values("Terminal 2", 000211);
insert into terminal values("North Satellite Terminal", 000211);
 
-- Doha --
insert into airport values (000301, "Hamad International Airport", 'Y', 'Y', 00030);
insert into terminal values("Terminal 1", 000301);
insert into terminal values("Terminal 2", 000301);
 
-- Dubai --
insert into airport values (000311, "Dubai International Airport", 'Y', 'Y', 00031);
insert into terminal values("Terminal 1", 000311);
insert into terminal values("Terminal 2", 000311);
insert into terminal values("Terminal 3", 000311);
 
-- Auckland --
insert into airport values (000401, "Auckland Airport", 'Y', 'Y', 00040);
insert into terminal values("International terminal", 000401);
insert into terminal values("Domestic terminal", 000401);
 
-- Wellington --
insert into airport values (000411, "Wellington International Airport", 'Y', 'Y', 00041);
insert into terminal values("Terminal 1", 000411);
 
-- Perth -- 
insert into airport values (000501, "Perth Airport", 'Y', 'Y', 00050);
insert into terminal values("Terminal 1", 000501);
insert into terminal values("Terminal 2", 000501);
insert into terminal values("Terminal 3", 000501);
insert into terminal values("Terminal 4", 000501);
 
insert into airport values (000502, "Jandakot Airport", 'N', 'N', 00050);
insert into terminal values("Terminal 1", 000502);
 
-- Brisbane --
insert into airport values (000511, "Brisbane Airport", 'Y', 'Y', 00051);
insert into terminal values("International Terminal", 000511);
insert into terminal values("Domestic Terminal", 000511);
 
-- Sydney --
insert into airport values (000521, "Sydney Airport", 'Y', 'Y', 00052);
insert into terminal values("Terminal 1", 000521);
insert into terminal values("Terminal 2", 000521);
insert into terminal values("Terminal 3", 000521);
 
-- Joburg --
insert into airport values (000601, "O. R. Tambo International Airport", 'Y', 'Y', 00060);
insert into terminal values("Terminal 1", 000601);
insert into terminal values("Terminal 2", 000601);
insert into terminal values("Terminal 3", 000601);
insert into terminal values("Terminal 4", 000601);
insert into terminal values("Terminal 5", 000601);
insert into terminal values("Terminal 6", 000601);
 
-- Cape Town --
insert into airport values (000611, "Cape Town International Airport", 'Y', 'N', 00061);
insert into terminal values("Terminal 1", 000611);
 
-- Marrakesh --
insert into airport values (000621, "Marrakesh Menara Airport", 'N', 'N', 00062);
insert into terminal values("Terminal 1", 000621);
 
-- Cairo --
insert into airport values (0006011, "Cairo International Airport", 'Y', 'N', 000601);
insert into terminal values("Terminal 1", 0006011);
insert into terminal values("Terminal 2", 0006011);
insert into terminal values("Terminal 3", 0006011);
insert into terminal values("Seasonal Flight Terminal", 0006011);
 
-- Addis Ababa --
insert into airport values (0006021, "Addis Ababa Bole International Airport", 'N', 'N', 000602);
insert into terminal values("Terminal 1", 0006021);
insert into terminal values("Terminal 2", 0006021);
 
-- Antanarivo --
insert into airport values (0006031, "Ivato International Airport", 'N', 'N', 000603);
insert into terminal values("Terminal 1", 0006031);
 
-- Nairobi --
insert into airport values (0006041, "Jomo Kenyatta International Airport", 'N', 'N', 000604);
insert into terminal values("Terminal 1", 0006041);
insert into terminal values("Terminal 2", 0006041);
 
-- Kathmandu --
insert into airport values (000701, "Tribhuvan International Airport", 'N', 'Y', 00070);
insert into terminal values("Terminal 1", 000701);
insert into terminal values("Terminal 2", 000701);
 
-- Denpasar --
insert into airport values (000711, "Ngurah Rai International Airport", 'Y', 'Y', 00071);
insert into terminal values("Terminal 1", 000711);
insert into terminal values("Terminal 2", 000711);
 
-- Bangkok -- 
insert into airport values (000721, "Suvarnabhumi Airport", 'N', 'N', 00072);
insert into terminal values("Terminal 1", 000721);
 
-- Colombo --
insert into airport values (0007031, "Bandaranaike International Airport", 'N', 'N', 000703);
insert into terminal values("Terminal 1", 0007031);
 
-- Vilnius --
insert into airport values (000801, "Vilnius Airport", 'Y', 'N', 00080);
insert into terminal values("Terminal 1", 000801);
 
-- Klaipeda --
insert into airport values (000811, "Palanga International Airport", 'Y', 'N', 00081);
insert into terminal values("South Terminal", 000811);
insert into terminal values("North Terminal", 000811);
 
-- St Petersburg --
insert into airport values (000821, "Pulkovo International Airport", 'Y', 'N', 00082);
insert into terminal values("Terminal 1", 000821);
 
-- Moscow --
insert into airport values (000831, "Domodedovo International Airport", 'Y', 'N', 00083);
insert into terminal values("Terminal 1", 000831);
 
insert into airport values (000832, "Sheremetyevo International Airport", 'Y', 'Y', 00083);
insert into terminal values("Terminal A", 000832);
insert into terminal values("Terminal B", 000832);
insert into terminal values("Terminal C", 000832);
insert into terminal values("Terminal D", 000832);
insert into terminal values("Terminal E", 000832);
insert into terminal values("Terminal F", 000832);
 
insert into airport values (000833, "Vnukovo International Airport", 'N', 'N', 00083);
insert into terminal values("Terminal A", 000833);
insert into terminal values("Terminal D", 000833);
 
-- Kiev --
insert into airport values (000841, "Boryspil International Airport", 'Y', 'N', 00084);
insert into terminal values("Terminal B", 000841);
insert into terminal values("Terminal D", 000841);
 
-- Dublin --
insert into airport values (0008501, "Dublin Airport", 'Y', 'Y', 000850);
insert into terminal values("Terminal 1", 0008501);
insert into terminal values("Terminal 2", 0008501);
 
-- Edinburgh --
insert into airport values (0008511, "Edinburgh Airport", 'Y', 'Y', 000851);
insert into terminal values("Terminal 1", 0008511);
 
-- Glasgow --
insert into airport values (00085121, "Glasgow Airport", 'N', 'N', 0008512);
insert into terminal values("Terminal 1", 00085121);
insert into terminal values("Terminal 2", 00085121);
 
-- London --
insert into airport values (0008521, "Heathrow Airport", 'Y', 'Y', 000852);
insert into terminal values("Terminal 1", 0008521);
insert into terminal values("Terminal 2", 0008521);
insert into terminal values("Terminal 3", 0008521);
insert into terminal values("Terminal 4", 0008521);
insert into terminal values("Terminal 5", 0008521);
 
insert into airport values (0008522, "Gatwick Airport", 'Y', 'Y', 000852);
insert into terminal values("North Terminal", 0008522);
insert into terminal values("South Terminal", 0008522);
 
insert into airport values (0008523, "London Luton Airport", 'Y', 'Y', 000852);
insert into terminal values("Terminal 1", 0008523);
 
insert into airport values (0008524, "London City Airport", 'N', 'N', 000852);
insert into terminal values("Terminal 1", 0008524);
 
-- Milan --
insert into airport values (0008531, "Milano Malpensa Airport", 'Y', 'Y', 000853);
insert into terminal values("Terminal 1", 0008531); 
insert into terminal values("Terminal 2", 0008531); 
 
insert into airport values (0008532, "Linate Airport", 'Y', 'N', 000853);
insert into terminal values("Terminal 1", 0008532); 
 
insert into airport values (0008533, "Orio al Serio International Airport", 'N', 'N', 000853);
insert into terminal values("Terminal 1", 0008533); 
 
-- Berlin --
insert into airport values (0008541, "Berlin Brandenburg Airport", 'Y', 'Y', 000854);
insert into terminal values("Terminal 1", 0008541); 
insert into terminal values("Terminal 1", 0008541); 
 
insert into airport values (0008542, "Berlin Schönefeld Airport", 'Y', 'Y', 000854);
insert into terminal values("Terminal A", 0008542); 
insert into terminal values("Terminal B", 0008542); 
insert into terminal values("Terminal C", 0008542); 
insert into terminal values("Terminal D", 0008542); 
 
insert into airport values (0008543, "Berlin Tegel Airport", 'Y', 'Y', 000854);
insert into terminal values("Terminal A", 0008543); 
insert into terminal values("Terminal B", 0008543); 
insert into terminal values("Terminal C", 0008543); 
insert into terminal values("Terminal D", 0008543);

-- Dresden --
insert into airport values (0008551, "Dresden Airport", 'N', 'N', 000855);
insert into terminal values("Terminal 1", 0008551);

-- Munich --
insert into airport values (0008561, "Munich Airport", 'N', 'N', 000856);
insert into terminal values("Terminal 1", 0008561);
insert into terminal values("Terminal 2", 0008561);
insert into terminal values("Terminal 2 Satellite", 0008561);

-- Linz --
insert into airport values (0008571, "Blue Danube Airport Linz", 'N', 'N', 000857);
insert into terminal values("Terminal 1", 0008571);

-- Vienna --
insert into airport values (0008581, "Vienna International Airport", 'Y', 'N', 000858);
insert into terminal values("Terminal 1", 0008581);
insert into terminal values("Terminal 1A", 0008581);
insert into terminal values("Terminal 2", 0008581);
insert into terminal values("Terminal 3", 0008581);

-- Budapest --
insert into airport values (0008591, "Budapest Ferenc Liszt International Airport", 'Y', 'Y', 000858);
insert into terminal values("Terminal 1", 0008591);
insert into terminal values("Terminal 2A", 0008591);
insert into terminal values("Terminal 2B", 0008591);

-- Prague -- 
insert into airport values (0008601, "Václav Havel Airport Prague", 'Y', 'Y', 000860);
insert into terminal values("Terminal 1", 0008601); # Flights outside the Schengen area
insert into terminal values("Terminal 2", 0008601); # Flights within the Schengen area
insert into terminal values("Terminal 3", 0008601); # Private and charter flights
insert into terminal values("Terminal 4", 0008601); # VIP and state visits

-- Amsterdam --
insert into airport values (00086011, "Amsterdam Airport Schiphol", 'Y', 'Y', 0008601);
insert into terminal values("Terminal 1", 00086011); 
insert into terminal values("Terminal 2", 00086011); 
insert into terminal values("Terminal 3", 00086011); 

-- Eindhoven --
insert into airport values (0008611, "Eindhoven Airport", 'N', 'Y', 000861);
insert into terminal values("Terminal 1", 0008611); 

-- Utrecht --
# Only way to get to Utrecht is through Amsterdam 
insert into airport values (0008621, "Amsterdam Airport Schiphol", 'N', 'Y', 0008601);
insert into terminal values("Terminal 1", 0008621); 
insert into terminal values("Terminal 2", 0008621); 
insert into terminal values("Terminal 3", 0008621);

-- Riga --
insert into airport values (0008631, "Riga International Airport", 'N', 'Y', 000863);
insert into terminal values("Terminal A", 0008631); 
insert into terminal values("Terminal B", 0008631); 
insert into terminal values("Terminal C", 0008631); 

-- Krakow --
insert into airport values (0008641, "John Paul II International Airport Kraków–Balice", 'N', 'N', 000864);
insert into terminal values("Terminal 1", 0008641); 

-- Reykjavik --
insert into airport values (0008651, "Reykjavík Airport", 'N', 'N', 000865);
insert into terminal values("Terminal 1", 0008651); 
insert into terminal values("Terminal 2", 0008651); 

insert into airport values (0008652, "Keflavík International Airport", 'N', 'N', 000865);
insert into terminal values("Terminal 1", 0008652); 

-- Porto --
insert into airport values (0008661, "Porto Airport", 'N', 'N', 000866);
insert into terminal values("Terminal 1", 0008661); 

-- Istanbul --
insert into airport values (0008671, "Istanbul Atatürk Airport", 'Y', 'Y', 000867);
insert into terminal values("Terminal 1", 0008671); 
insert into terminal values("Terminal 2", 0008671); 

-- Barcelona --
insert into airport values (0008681, "Barcelona–El Prat Airport", 'Y', 'Y', 000868);
insert into terminal values("Terminal 1", 0008681); 
insert into terminal values("Terminal 2", 0008681); 

-- Malaga --
insert into airport values (0008691, "Málaga Airport", 'Y', 'N', 000869);
insert into terminal values("Terminal 1", 0008691); 
insert into terminal values("Terminal 2", 0008691); 
insert into terminal values("Terminal 3", 0008691); 

-- Nice --
insert into airport values (0008701, "Nice Côte d'Azur Airport", 'Y', 'Y', 000870);
insert into terminal values("Terminal 1", 0008701); 
insert into terminal values("Terminal 2", 0008701); 

-- Paris --
insert into airport values (0008711, "Charles de Gaulle Airport", 'Y', 'Y', 000871);
insert into terminal values("Terminal 1", 0008711); 
insert into terminal values("Terminal 2", 0008711); 
insert into terminal values("Terminal 3", 0008711);

-- Santiago --
insert into airport values (000901, "Comodoro Arturo Merino Benítez International Airport", 'Y', 'Y', 00090);
insert into terminal values("Terminal 1", 000901); 

-- Buenos Aires --
insert into airport values (0009011, "Aeroparque Jorge Newbery", 'Y', 'Y', 000901);
insert into terminal values("Terminal 1", 0009011); 
insert into terminal values("Terminal 2", 0009011); 
insert into terminal values("Terminal 3", 0009011); 
insert into terminal values("Terminal 4", 0009011); 

insert into airport values (0009012, "Ministro Pistarini International Airport", 'Y', 'Y', 000901);
insert into terminal values("Terminal A", 0009012); 
insert into terminal values("Terminal B", 0009012); 
insert into terminal values("Terminal C", 0009012); 

-- Rio --
insert into airport values (0009021, "Rio de Janeiro–Galeão International Airport", 'Y', 'Y', 000902);
insert into terminal values("Terminal 1", 0009021); 
insert into terminal values("Terminal 2", 0009021);

-- Salvador --
insert into airport values (0009031, "Deputado Luís Eduardo Magalhães International Airport", 'N', 'N', 000903);
insert into terminal values("Terminal 1", 0009031); 

-- New York --
insert into airport values (001001, "LaGuardia Airport", 'Y', 'Y', 00100);
insert into terminal values("Terminal A", 001001); 
insert into terminal values("Terminal B", 001001); 
insert into terminal values("Terminal C", 001001); 
insert into terminal values("Terminal D", 001001); 

insert into airport values (001002, "John F. Kennedy International Airport", 'Y', 'Y', 00100);
insert into terminal values("Terminal 1", 001002); 
insert into terminal values("Terminal 2", 001002); 
insert into terminal values("Terminal 4", 001002); 
insert into terminal values("Terminal 5", 001002); 
insert into terminal values("Terminal 7", 001002); 
insert into terminal values("Terminal 8", 001002); 

insert into airport values (001003, "Newark Liberty International Airport", 'Y', 'N', 00100);
insert into terminal values("Terminal A", 001003); 
insert into terminal values("Terminal B", 001003);
insert into terminal values("Terminal C", 001003);

-- San Fran --
insert into airport values (001101, "San Francisco International Airport", 'Y', 'Y', 00110);
insert into terminal values("Terminal 1", 001101); 
insert into terminal values("Terminal 2", 001101); 
insert into terminal values("Terminal 3", 001101); 
insert into terminal values("International Terminal", 001101);  

-- Boston --
insert into airport values (001201, "Logan International Airport", 'N', 'Y', 00120);
insert into terminal values("Terminal A", 001201); 
insert into terminal values("Terminal B", 001201); 
insert into terminal values("Terminal C", 001201); 
insert into terminal values("Terminal E", 001201); 

-- Toronto --
insert into airport values (001301, "Toronto Pearson International Airport", 'N', 'N', 00130);
insert into terminal values("Terminal 1", 001301); 
insert into terminal values("Terminal 3", 001301); 
insert into terminal values("Infield Terminal", 001301); 

-- Tel Aviv --
insert into airport values (000321, "Ben Gurion Airport", 'Y', 'Y', 00032);
insert into terminal values("Terminal 1", 000321); 
insert into terminal values("Terminal 3", 000321); 

-- Rome --
insert into airport values (00085311, "Leonardo da Vinci–Fiumicino Airport", 'Y', 'Y', 0008531);
insert into terminal values("Terminal 1", 00085311); 
insert into terminal values("Terminal 2", 00085311); 
insert into terminal values("Terminal 3", 00085311); 
insert into terminal values("Terminal 5", 00085311); 

-- Beijing --
insert into airport values (0002111, "Beijing Capital International Airport", 'Y', 'Y', 000211);
insert into terminal values("Terminal 1", 0002111); 
insert into terminal values("Terminal 2", 0002111); 
insert into terminal values("Terminal 3", 0002111); 

-- Beirut --
insert into airport values (000331, "Beirut–Rafic Hariri International Airport", 'N', 'Y', 00033);
insert into terminal values("West Wing", 000331); 
insert into terminal values("East Wing", 000331); 

-- Bacau -- 
insert into airport values (0008411, "Bacău International Airport", 'N', 'Y', 000841);
insert into terminal values("Terminal 1", 0008411); 

-- Toulouse --
insert into airport values (0008721, "Toulouse–Blagnac Airport", 'N', 'Y', 000872);
insert into terminal values("Terminal 1", 0008721); 

-- Zanzibar --
insert into airport values (0006051, "Abeid Amani Karume International Airport", 'N', 'N', 000605);
insert into terminal values("Terminal 1", 0006051); 
insert into terminal values("Terminal 2", 0006051); 

-- Sharm El Sheikh -- 
insert into airport values (00060111, "Abeid Amani Karume International Airport", 'Y', 'Y', 0006011);
insert into terminal values("Terminal 1", 00060111); 
insert into terminal values("Terminal 2", 00060111); 

-- Odessa --
insert into airport values (0008421, "Odessa International Airport", 'N', 'N', 000842);
insert into terminal values("Terminal 1", 0008421); 

-- Manila -- 
insert into airport values (00087011, "Ninoy Aquino International Airport", 'Y', 'Y', 0008701);
insert into terminal values("Terminal 1", 00087011); 
insert into terminal values("Terminal 2", 00087011); 
insert into terminal values("Terminal 3", 00087011); 
insert into terminal values("Terminal 4", 00087011); 
insert into terminal values("Terminal 5", 00087011); 

-- Kuala Lumpur -- 
insert into airport values (000741, "Kuala Lumpur International Airport", 'Y', 'N', 00074);
insert into terminal values("Terminal 1", 000741); 
insert into terminal values("Terminal 2", 000741); 
insert into terminal values("Terminal 3", 000741); 

-- Zagreb -- 
insert into airport values (0008811, "Zagreb Airport", 'Y', 'N', 000881);
insert into terminal values("Terminal 1", 0008811); 

-- Athens --
insert into airport values (0008821, "Athens International Airport", 'Y', 'N', 000882);
insert into terminal values("Main Terminal", 0008821); 
insert into terminal values("Satellite Terminal", 0008821); 

-- Thessaloniki --
insert into airport values (00088221, "Thessaloniki Airport", 'Y', 'Y', 0008822);
insert into terminal values("Main Terminal", 00088221); 

-- Frankfurt -- 
insert into airport values (000854011, "Frankfurt Airport", 'Y', 'Y', 00085401);
insert into terminal values("Terminal 1", 000854011); 
insert into terminal values("Terminal 2", 000854011); 

-- Bahamas -- 
insert into airport values (001401, "Lynden Pindling International Airport", 'N', 'Y', 00140);
insert into terminal values("Terminal 1", 001401); 
insert into terminal values("Terminal 2", 001401); 

-- Phuket -- 
insert into airport values (0007211, "Phuket International Airport", 'Y', 'Y', 000721);
insert into terminal values("Terminal 1", 0007211); 
insert into terminal values("Terminal 2", 0007211); 

-- Dehli --  
insert into airport values (000751, "Indira Gandhi International Airport", 'Y', 'Y', 00075);
insert into terminal values("Terminal 1", 000751); 
insert into terminal values("Terminal 2", 000751); 
insert into terminal values("Terminal 3", 000751); 