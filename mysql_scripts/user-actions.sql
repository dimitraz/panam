# Checking arrival status of a given flight
select 
	flight_id as 'Flight number', name as 'From airport', arrival_time as 'Arriving', status as 'Status' 
    from flight, airport
    where flight_id = 04377
    and to_airport_id = airport_id;

# All airline flights departing on a specific date
select
	flight_id as 'Flight number',city as 'From', name as 'From airport', DATE_FORMAT(departure_time, '%Y-%m-%d %h:%m') as 'Departing', status as 'Status' 
    from flight, airport, destination
	where from_airport_id = airport_id
    and airport_destination_id = destination_id
    and DATE_FORMAT(departure_time, '%Y-%m-%d')  = '2017-06-11';

# All airline flights arriving on a specific date
select
	flight_id as 'Flight number',city as 'From', name as 'From airport', DATE_FORMAT(arrival_time, '%Y-%m-%d %h:%m') as 'Arriving', status as 'Status' 
    from flight, airport, destination
	where to_airport_id = airport_id
    and airport_destination_id = destination_id
    and DATE_FORMAT(arrival_time, '%Y-%m-%d')  = '2017-06-11';

# All flights arriving at a specific airport
select 
	flight_id as 'Flight number', name as 'From airport', DATE_FORMAT(arrival_time, '%Y-%m-%d %h:%m') as 'Arriving', status as 'Status' 
    from flight, airport
    where to_airport_id = airport_id
    and name = 'Incheon International Airport';
    
# All flights departing from a specific airport
select 
	flight_id as 'Flight number', name as 'From airport', DATE_FORMAT(departure_time, '%Y-%m-%d %h:%m') as 'Departing', status as 'Status' 
    from flight, airport
    where from_airport_id = airport_id
    and name = 'Domodedovo International Airport ';
    
