use airline;
 
insert into destination values (00010, "Osaka", "Japan");
insert into destination values (00011, "Kyoto", "Japan");
insert into destination values (00012, "Tokyo", "Japan");
insert into destination values (00020, "Seoul", "South Korea");
insert into destination values (00021, "Hong Kong", "China");
insert into destination values (000211, "Beijing", "China");
insert into destination values (00030, "Doha", "Qatar");
insert into destination values (00031, "Dubai", "UAE");
insert into destination values (00032, "Tel Aviv", "Israel");
insert into destination values (00033, "Beirut", "Lebanon");
 
insert into destination values (00040, "Auckland", "New Zealand");
insert into destination values (00041, "Wellington", "New Zealand");
insert into destination values (00050, "Perth", "Australia");
insert into destination values (00051, "Brisbane", "Australia");
insert into destination values (00052, "Sydney", "Australia");
 
insert into destination values (00060, "Johannesburg", "South Africa");
insert into destination values (00061, "Cape Town", "South Africa");
insert into destination values (00062, "Marrakesh", "Morocco");
insert into destination values (000601, "Cairo", "Egypt");
insert into destination values (0006011, "Sharm El Sheikh", "Egypt");
insert into destination values (000602, "Addis Ababa", "Ethiopia");
insert into destination values (000603, "Antananarivo", "Madagascar");
insert into destination values (000604, "Nairobi", "Kenya");
insert into destination values (000605, "Zanzibar City", "Zanzibar");
 
insert into destination values (00070, "Kathmandu", "Nepal");
insert into destination values (00071, "Denpasar", "Bali, Indonesia");
insert into destination values (00072, "Bangkok", "Thailand");
insert into destination values (000721, "Phuket", "Thailand");
insert into destination values (000703, "Colombo", "Sri Lanka");
insert into destination values (00074, "Kuala Lumpur", "Malaysia");
insert into destination values (00075, "Dehli", "India");
 
insert into destination values (00080, "Vilnius", "Lithuania");
insert into destination values (00081, "Klaipeda", "Lithuania");
insert into destination values (00082, "Saint Petersburg", "Russia");
insert into destination values (00083, "Moscow", "Russia");
insert into destination values (00084, "Kiev", "Ukraine");
insert into destination values (000842, "Odessa", "Ukraine");
insert into destination values (000841, "Bacau", "Romania");
insert into destination values (000850, "Dublin", "Ireland");
insert into destination values (000851, "Edinburgh", "Scotland");
insert into destination values (0008512, "Glasgow", "Scotland");
insert into destination values (000852, "London", "England");
insert into destination values (000853, "Milan", "Italy");
insert into destination values (0008531, "Rome", "Italy");
insert into destination values (000854, "Berlin", "Germany");
insert into destination values (000855, "Dresden", "Germany");
insert into destination values (000856, "Munich", "Germany");
insert into destination values (00085401, "Frankfurt", "Germany");
insert into destination values (000857, "Linz", "Austria");
insert into destination values (000858, "Vienna", "Austria");
insert into destination values (000859, "Budapest", "Hungary");
insert into destination values (000860, "Prague", "Czech Republic");
insert into destination values (0008601, "Amsterdam", "The Netherlands");
insert into destination values (000861, "Eindhoven", "The Netherlands");
insert into destination values (000862, "Utrecht", "The Netherlands");
 
insert into destination values (000863, "Riga", "Latvia");
insert into destination values (000864, "Krakow", "Poland");
insert into destination values (000865, "Reykjavic", "Iceland");
insert into destination values (000866, "Porto", "Portugal");
insert into destination values (000867, "Istanbul", "Turkey");
insert into destination values (000868, "Barcelona", "Spain");
insert into destination values (000869, "Malaga", "Spain");
insert into destination values (000870, "Nice", "France");
insert into destination values (000871, "Paris", "France");
insert into destination values (000872, "Toulouse", "France");
insert into destination values (0008701, "Manila", "Philippines");
insert into destination values (000881, "Zagreb", "Croatia");
insert into destination values (000882, "Athens", "Greece");
insert into destination values (0008822, "Thessaloniki", "Greece");
 
insert into destination values (00090, "Santiago", "Chile");
insert into destination values (000901, "Buenos Aires", "Argentina");
insert into destination values (000902, "Rio de Janeiro", "Brazil");
insert into destination values (000903, "Salvador", "Brazil");
 
insert into destination values (00100, "New York", "America");
insert into destination values (00110, "San Francisco", "America");
insert into destination values (00120, "Boston", "America");
insert into destination values (00130, "Toronto", "Canada");
insert into destination values (00140, "Nassau", "Bahamas");
