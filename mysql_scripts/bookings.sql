use airline;
   
   delimiter $
   create trigger check_booking before insert on booking
	for each row 
		begin 
			if new.checked_in not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'checked_in : not Y or N';
			end if;
			if new.return not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'return: not Y or N';
			end if;
			if new.priority_boarding not in ('Y','N') then
			  signal sqlstate '45000' set message_text = 'priority_boarding: not Y or N';
			end if;
	end$
	delimiter ;
    
  #trigger checks
  #insert into booking values(00444, '2017-01-12', 'T',256.20 , 'Y', 0, '0026d', 'N', 01001,'piperflower');
  #insert into booking values(00445, '2017-01-12', 'Y',256.20 , 'T', 0, '0025d', 'N', 01001,'piperflower');
  #insert into booking values(00446, '2017-01-12', 'Y',256.20 , 'Y', 0, '0024d', 'T', 01001,'piperflower');
   
  insert into booking values(00000, '2017-01-12', 'N',256.20 , 'Y', 0, '0026d', 'N', 01001,'piperflower');
  insert into booking values(00001, '2017-02-10', 'N',240.80 , 'N', 0, '0025d', 'N', 01001,'piperflower');
  insert into booking values(00002, '2016-12-23', 'N',336.50 , 'N', 0, '0024d', 'Y', 01001,'curlingvoiceless');
  insert into booking values(00003, '2017-02-01', 'N',430.0  , 'Y', 0, '0022d', 'Y', 01001,'plonkgallop');
  insert into booking values(00004, '2017-03-14', 'N',306.50 , 'N', 0, '0021d', 'N', 01001,'lucrumitch');
  
  insert into booking values(00005, '2017-02-14', 'N',336.50 , 'Y', 0, '0020d', 'N', 01005,'needleschampagne');
  insert into booking values(00006, '2017-02-14', 'N',240.80  , 'N', 0, '0019d', 'N', 01005,'needleschampagne');
  insert into booking values(00007, '2017-02-14', 'N',336.50 , 'Y', 0, '0018d', 'Y', 01005,'needleschampagne');
  insert into booking values(00008, '2017-01-12', 'N',430.0 , 'Y', 0, '0017d', 'N', 01005,'barbaro');
  insert into booking values(00009, '2017-01-10', 'N',240.80  , 'Y', 0, '0016d', 'Y', 01005,'vietnamesedevastated');
  
  insert into booking values(00010, '2017-01-07', 'N',250.80  , 'Y', 0, '0015d', 'N', 01007,'jackday');
  insert into booking values(00011, '2017-01-01', 'N',524.20 , 'N', 0, '0014d', 'N', 01007,'jackday');
  insert into booking values(00012, '2017-01-05', 'N',525.40 , 'N', 0, '0014d', 'N', 01007,'galliumflogging');
  insert into booking values(00013, '2017-01-12', 'N',437.0 , 'N', 0, '0013d', 'Y', 01007,'vacancylondon');
  insert into booking values(00014, '2017-01-05', 'N',435.0 , 'Y', 0, '0012d', 'Y', 01007,'filibusterpaw');
  
  insert into booking values(00015, '2017-01-10', 'N',435.0 , 'Y', 0, '0011d', 'N', 01010,'tattooedlookup');
  insert into booking values(00016, '2017-03-14', 'N',432.0 , 'Y', 0, '0010d', 'N', 01010,'deadeyelicker');
  insert into booking values(00017, '2017-03-14', 'N',431.0 , 'Y', 0, '0009d', 'Y', 01010,'herbalwizard');
  insert into booking values(00018, '2017-03-13', 'N',520.20 , 'Y', 0, '0008d', 'N', 01010,'bandedfalloch');
  insert into booking values(00019, '2017-03-14', 'N',524.20 , 'N', 0, '0005d', 'N', 01010,'finicalsores');
  
  insert into booking values(00020, '2017-03-14', 'N',545.20 , 'N', 0, '0007d', 'N', 01012,'ultravioletinspect');
  insert into booking values(00021, '2017-03-12', 'N',240.50  , 'Y', 0, '0004d', 'N', 01012,'lucrumitch');
  insert into booking values(00022, '2017-02-14', 'N',220.20  , 'Y', 0, '0003d', 'N', 01012,'reelingintended');
  insert into booking values(00023, '2017-01-05', 'N',525.20 , 'Y', 0, '0002d', 'N', 01012,'shortcrustconcurrent');
  insert into booking values(00024, '2017-01-05', 'N',240.80  , 'Y', 0, '0001d', 'N', 01012,'exaltedjasmine');
  
  insert into booking values(00025, '2017-02-14', 'N',525.20 , 'Y', 0, '0002d', 'N', 01037,'therapyinvents');
  insert into booking values(00026, '2016-08-17', 'N',540.20 , 'Y', 0, '0001d', 'N', 01037,'telluriumloan');
  insert into booking values(00027, '2016-11-17', 'N',336.50 , 'N', 0, '0031c', 'N', 01037,'cupcaketomcat');
  insert into booking values(00028, '2017-03-14', 'N',336.50 , 'N', 0, '0030c', 'Y', 01037,'pilespapoose');
  insert into booking values(00029, '2017-03-18', 'N',390.50 , 'Y', 0, '0028c', 'Y', 01037,'plonkgallop');

  insert into booking values(00030, '2017-03-15', 'N',309.50 , 'Y', 0, '0029c', 'N', 02001,'pearlpure');
  insert into booking values(00031, '2016-11-07', 'N',336.50 , 'Y', 0, '0027c', 'N', 02001,'ringedignorant');
  insert into booking values(00032, '2016-08-17', 'N',336.50 , 'N', 0, '0026c', 'Y', 02001,'allagalkina');
  insert into booking values(00033, '2016-08-17', 'N',240.80  , 'N', 0, '0025c', 'Y', 02001,'salmonsjourney');
  insert into booking values(00034, '2016-08-17', 'N',336.50 , 'Y', 0, '0023c', 'Y', 02001,'brandysparky');
  insert into booking values(00035, '2016-07-07', 'N',240.80  , 'Y', 0, '0022c', 'N', 02001,'knivesbeacons');
  
  insert into booking values(00036, '2016-07-07', 'N',336.50 , 'Y', 0, '0021c', 'N', 02002,'counttiresome');
  insert into booking values(00037, '2016-07-07', 'N',240.80  , 'Y', 0, '0020c', 'N', 02002,'shapetablet');
  insert into booking values(00038, '2016-08-15', 'N',533.20 , 'Y', 0, '0019c', 'N', 02002,'cadgeshovel');
  insert into booking values(00039, '2016-07-07', 'N',555.50 , 'Y', 0, '0018c', 'N', 02002,'vietnamesedevastated');
  insert into booking values(00040, '2016-08-11', 'N',240.80  , 'Y', 0, '0017c', 'N', 02002,'vietnamesedevastated');
  
  insert into booking values(00041, '2016-07-07', 'N',525.20 , 'N', 0, '0016c', 'N', 02005,'adamantsaphire');
  insert into booking values(00042, '2016-08-17', 'N',552.25 , 'N', 0, '0013c', 'N', 02005,'donfirm');
  insert into booking values(00043, '2016-08-17', 'N',525.85 , 'Y', 0, '0015c', 'N', 02005,'sniffshowtime');
  insert into booking values(00044, '2016-07-01', 'N',533.65 , 'Y', 0, '0014c', 'N', 02005,'limeyrear');
  insert into booking values(00045, '2016-07-08', 'N',525.35 , 'Y', 0, '0013c', 'N', 02005,'nevadamerced');
  
  insert into booking values(00046, '2017-03-01', 'N',730.0 , 'Y', 0, '0012c', 'N', 01033,'limeyrear');
  insert into booking values(00047, '2017-02-02', 'N',230.0 , 'Y', 0, '0012c', 'Y', 01033,'starryplanum');
  insert into booking values(00048, '2017-03-01', 'N',230.0 , 'Y', 0, '0011c', 'Y', 01033,'mothertiger');
  insert into booking values(00049, '2017-02-02', 'N',430.0 , 'N', 0, '0010c', 'Y', 01033,'bookmarkgaiter');
  insert into booking values(00050, '2017-02-02', 'N',430.0 , 'N', 0, '0005c', 'Y', 01033,'repellentquokka');
  
  insert into booking values(00051, '2017-03-01', 'N',220.80  , 'N', 0, '0005c', 'N', 01032,'kenoraeris');
  insert into booking values(00052, '2016-07-17', 'N',240.80  , 'N', 0, '0005c', 'N', 01032,'coinedhomeless');
  insert into booking values(00053, '2017-03-01', 'N',220.80  , 'Y', 0, '0004c', 'N', 01032,'companionwayoral');
  insert into booking values(00054, '2016-12-23', 'N',310.10 , 'Y', 0, '0003c', 'N', 01032,'gruntioxhost');
  insert into booking values(00055, '2016-07-13', 'N',336.50 , 'Y', 0, '0003c', 'N', 01032,'pricksamsung');
  
  insert into booking values(00056, '2016-12-23', 'N',430.0 , 'Y', 0, '0002c', 'Y', 01031,'smartiechristie');
  insert into booking values(00057, '2016-11-05', 'N',430.0 , 'Y', 0, '0001c', 'Y', 01031,'assetsrowen');
  insert into booking values(00058, '2017-02-02', 'N',420.20 , 'Y', 0, '0019b', 'N', 01031,'shortywagtail');
  insert into booking values(00059, '2017-01-12', 'N',337.50 , 'N', 0, '00181', 'N', 01031,'rodeoswiftsilver');
  insert into booking values(00060, '2017-01-12', 'N',376.50 , 'N', 0, '0017b', 'Y', 01031,'cetusarrows');
  
  insert into booking values(00061, '2017-02-02', 'N',350.50 , 'Y', 0, '0016b', 'N', 01038,'rodeoswiftsilver');
  insert into booking values(00062, '2017-01-12', 'N',336.50 , 'Y', 0, '0015b', 'N', 01038,'smashingdyno');
  insert into booking values(00063, '2017-01-12', 'N',240.80  , 'Y', 0, '0014b', 'N', 01038,'smashingdyno');
  insert into booking values(00064, '2017-02-12', 'N',245.85  , 'N', 0, '0013b', 'N', 01038,'smashingdyno');
  insert into booking values(00065, '2017-01-11', 'N',240.80  , 'N', 0, '0012b', 'N', 01038,'chocolatestrike');
  
  insert into booking values(00066, '2017-02-02', 'N',240.80  , 'Y', 0, '0013b', 'N', 02007,'batteredpa');
  insert into booking values(00067, '2017-02-02', 'N',240.55  , 'Y', 0, '0011b', 'Y', 02007,'cetusarrows');
  insert into booking values(00068, '2016-11-05', 'N',1002.0 , 'N', 0, '0010b', 'Y', 02007,'nearbuddy');
  insert into booking values(00069, '2016-08-15', 'N',1200.60 , 'Y', 0, '0009b', 'Y', 02007,'belaybishop');
  insert into booking values(00070, '2015-12-10', 'N',90.0 , 'Y', 0, '0009b', 'N', 02007,'sistonperfumed');
  
  insert into booking values(00071, '2017-03-01', 'N',65.80 , 'Y', 0, '0009b', 'N', 02008,'charlycollege');
  insert into booking values(00072, '2017-03-01', 'N',25.20 , 'Y', 0, '0008b', 'N', 02008,'meringuesocks');
  insert into booking values(00073, '2017-03-01', 'N',336.10 , 'N', 0, '0007b', 'N', 02008,'glenmorehanging');
  insert into booking values(00074, '2016-08-17', 'N',331.50 , 'N', 0, '0006b', 'N', 02008,'polishedsoupy');
  insert into booking values(00075, '2016-08-15', 'N',331.50 , 'N', 0, '0005b', 'N', 02008,'sourdoughdiamox');
  
  insert into booking values(00076, '2016-09-05', 'N',27.90 , 'N', 0, '0004b', 'N', 02009,'helveticaborle');
  insert into booking values(00077, '2016-09-05', 'N',470.0 , 'Y', 0, '0004b', 'N', 02009,'chipsolo');
  insert into booking values(00078, '2016-09-03', 'N',410.10 , 'Y', 0, '0003b', 'N', 02009,'foresterrhinitis');
  insert into booking values(00079, '2017-01-12', 'N',430.70 , 'Y', 0, '0003b', 'N', 02009,'klickitatreturns');
  insert into booking values(00080, '2016-09-09', 'N',66.60 , 'Y', 0, '0002b', 'N', 02009,'chaseclogs');
  
  insert into booking values(00081, '2015-12-10', 'N',430.30 , 'Y', 0, '0001b', 'N', 02003,'chaseclogs');
  insert into booking values(00082, '2016-09-05', 'N',245.80  , 'N', 0, '0029a', 'Y', 02003,'chaseclogs');
  insert into booking values(00083, '2016-09-03', 'N',240.85  , 'N', 0, '0028a', 'Y', 02003,'pepperscusp');
  insert into booking values(00084, '2016-12-23', 'N',99.35 , 'Y', 0, '0027a', 'N', 02003,'variscanbaggy');
  insert into booking values(00085, '2016-11-05', 'N',336.25 , 'Y', 0, '0020a', 'N', 02003,'pepperscusp');
  
  insert into booking values(00086, '2017-01-12', 'N',336.30 , 'Y', 0, '0019a', 'N', 02009,'blackiearmenian');
  insert into booking values(00087, '2017-01-12', 'N',333.50 , 'Y', 0, '0018a', 'N', 02009,'blackiearmenian');
  insert into booking values(00088, '2017-01-12', 'N',37.50 , 'Y', 0, '0017a', 'N', 02009,'pinterestdearg');
  insert into booking values(00089, '2016-09-03', 'N',33.90 , 'Y', 0, '0016a', 'N', 02009,'cetusarrows');
  insert into booking values(00090, '2016-09-03', 'N',430.70 , 'N', 0, '0016a', 'Y', 02009,'pinterestdearg');
  
  insert into booking values(00100, '2016-09-03', 'N',430.40 , 'N', 0, '0015a', 'Y', 02019,'vaulttia');
  insert into booking values(00101, '2017-01-12', 'N',430.10 , 'Y', 0, '0014a', 'N', 02019,'jeassicapears');
  insert into booking values(00102, '2017-02-14', 'N',70.96 , 'Y', 0, '0013a', 'N', 02019,'jeassicapears');
  insert into booking values(00103, '2017-03-01', 'N',10.10 , 'Y', 0, '0012a', 'N', 02019,'jeassicapears');
  insert into booking values(00104, '2016-11-03', 'N',440.80  , 'N', 0, '0011a', 'N', 02019,'vaulttia');
  
  insert into booking values(00105, '2016-11-03', 'N',244.80  , 'N', 0, '0010a', 'N', 02020,'knowingfea');
  insert into booking values(00106, '2016-11-05', 'N',240.70  , 'N', 0, '0009a', 'N', 02020,'toucanfrankel');
  insert into booking values(00107, '2016-08-17', 'N',340.90  , 'N', 0, '0008a', 'N', 02020,'toucanfrankel');
  insert into booking values(00108, '2016-08-01', 'N',336.50 , 'N', 0, '0008a', 'Y', 02020,'beenhank');
  insert into booking values(00109, '2016-08-17', 'N',236.50 , 'N', 0, '0006a', 'Y', 02020,'beenhank');
  
  insert into booking values(00110, '2016-11-06', 'N',80.80 , 'N', 0, '0007a', 'Y', 02022,'knowingfea');
  insert into booking values(00111, '2016-11-03', 'N',85.20 , 'N', 0, '0007a', 'N', 02022,'fastidioushated');
  insert into booking values(00112, '2016-11-03', 'N',336.50 , 'N', 0, '0006a', 'N', 02022,'lloydflaps');
  insert into booking values(00113, '2016-12-23', 'N',336.50 , 'Y', 0, '0005a', 'N', 02022,'lloydflap');
  insert into booking values(00114, '2016-11-30', 'N',939.50 , 'Y', 0, '0004a', 'N', 02022,'phonemoondance');
  
  insert into booking values(00115, '2016-11-13', 'N',240.80  , 'Y', 0, '0003a', 'N', 02025,'muscaaurora');
  insert into booking values(00116, '2016-11-03', 'N',110.10 , 'Y', 0, '0002a', 'N', 02025,'muscaaurora');
  insert into booking values(00117, '2016-12-23', 'N',450.05 , 'Y', 0, '0001a', 'N', 02025,'pinterestdearg');
  insert into booking values(00230, '2016-01-12', 'N',45.50 , 'Y', 0, '0030e', 'N', 02025,'phonemoondance');
  insert into booking values(00231, '2016-02-10', 'N',65.30 , 'Y', 0, '0030e', 'N', 02025,'phonemoondance');
  
  insert into booking values(00232, '2017-12-23', 'N',90.70 , 'N', 0, '0030e', 'N', 02027,'lloydflap');
  insert into booking values(00233, '2016-02-01', 'N',99.00 , 'N', 0, '0029e', 'N', 02027,'muscaaurora');
  insert into booking values(00234, '2016-03-14', 'N',50.0 , 'Y', 0, '0028e', 'N', 02027,'variscanbaggy');
  insert into booking values(00235, '2016-02-14', 'N',25.0 , 'N', 0, '0026e', 'N', 02027,'harleyollie');
  insert into booking values(00236, '2016-02-14', 'N',430.05 , 'N', 0, '0025e', 'N', 02027,'harleyollie');
  
  insert into booking values(00237, '2016-02-14', 'N',410.05 , 'Y', 0, '0024e', 'N', 02030,'admissionsqueasy');
  insert into booking values(00238, '2016-01-12', 'N',396.50 , 'Y', 0, '0023e', 'N', 02030,'vaulttia');
  insert into booking values(00239, '2016-01-10', 'N',336.90 , 'Y', 0, '0022e', 'N', 02030,'spoolingbell');
  insert into booking values(00240, '2016-01-07', 'N',336.50 , 'Y', 0, '0022e', 'N', 02030,'spoolingbell');
  insert into booking values(00241, '2016-01-01', 'N',337.90 , 'Y', 0, '0020e', 'N', 02030,'admissionsqueasy');
  
  insert into booking values(00242, '2016-01-05', 'N',430.0 , 'N', 0, '0019e', 'Y', 02033,'admissionsqueasy');
  insert into booking values(00243, '2016-01-12', 'N',430.80 , 'N', 0, '0018e', 'N', 02033,'parisindent');
  insert into booking values(00244, '2016-01-05', 'N',480.80 , 'N', 0, '0018e', 'N', 02033,'parisindent');
  insert into booking values(00245, '2016-01-10', 'N',244.40  , 'N', 0, '0018e', 'N', 02033,'admissionsqueasy');
  insert into booking values(00246, '2016-03-14', 'N',240.80  , 'N', 0, '0015e', 'N', 02033,'admissionsqueasy');
  
  insert into booking values(00247, '2016-03-14', 'N',240.80  , 'N', 0, '0013e', 'Y', 02034,'jeassicapears');
  insert into booking values(00248, '2016-03-13', 'N',440.80  , 'N', 0, '0012e', 'N', 02034,'donnapaul');
  insert into booking values(00249, '2016-03-14', 'N',100.0 , 'Y', 0, '0011e', 'N', 02034,'shampoominot');
  insert into booking values(00250, '2016-03-14', 'N',25.25 , 'Y', 0, '0010e', 'N', 02034,'grimeoutlying');
  insert into booking values(00251, '2016-03-12', 'N',55.50 , 'Y', 0, '0009e', 'N', 02034,'shampoominot');
  
  insert into booking values(00252, '2016-02-14', 'N',331.0 , 'Y', 0, '0007e', 'N', 02035,'whitelakebipedal');
  insert into booking values(00253, '2016-01-05', 'N',350.50 , 'Y', 0, '0006e', 'N', 02035,'whitelakebipedal');
  insert into booking values(00254, '2016-01-05', 'N',430.0 , 'N', 0, '0005e', 'Y', 02035,'grimeoutlying');
  insert into booking values(00255, '2016-02-14', 'N',430.0 , 'N', 0, '0005e', 'N', 02035,'grimeoutlying');
  insert into booking values(00256, '2017-08-17', 'N',430.0 , 'N', 0, '0001e', 'N', 02035,'donnapaul');
   
  insert into booking values(00200, '2017-02-12', 'N',83.45 , 'Y', 0, '0022f', 'N', 02040,'donnapaul');
  insert into booking values(00201, '2017-01-10', 'N',140.80  , 'Y', 0, '0024f', 'N', 02040,'shampoominot');
  insert into booking values(00202, '2016-11-23', 'N',240.80  , 'Y', 0, '0022f', 'N', 02040,'beverleychair');
  insert into booking values(00203, '2017-01-01', 'N',336.50 , 'Y', 0, '0020f', 'N', 02040,'beverleychair');
  insert into booking values(00204, '2017-02-14', 'N',336.50 , 'Y', 0, '0019f', 'N', 02040,'ecstaticdoubling');
  
  insert into booking values(00205, '2017-03-14', 'N',76.30 , 'Y', 0, '0018f', 'N', 02041,'ecstaticdoubling');
  insert into booking values(00206, '2017-01-14', 'N',75.20 , 'Y', 0, '0017f', 'N', 02041,'ecstaticdoubling');
  insert into booking values(00207, '2017-03-14', 'N',79.90 , 'N', 0, '0016f', 'N', 02041,'alkynefreeze');
  insert into booking values(00208, '2017-02-12', 'N',400.0 , 'N', 0, '0015f', 'N', 02041,'pharmacybestow');
  insert into booking values(00209, '2017-02-10', 'N',403.0 , 'N', 0, '0014f', 'Y', 02041,'pharmacybestow');
  
  insert into booking values(00210, '2017-01-03', 'N',441.80  , 'Y', 0, '0013f', 'N', 02045,'rawslight');
  insert into booking values(00211, '2017-01-02', 'N',240.80  , 'Y', 0, '0012f', 'N', 02045,'arnisdalepalace');
  insert into booking values(00212, '2017-02-05', 'N',120.0  , 'Y', 0, '0011f', 'N', 02045,'guarantees');
  insert into booking values(00213, '2017-02-12', 'N',240.80  , 'N', 0, '0010f', 'N', 02045,'guarantees');
  insert into booking values(00214, '2017-02-05', 'N',196.80  , 'N', 0, '0009f', 'Y', 02045,'arnisdalepalace');
  
  insert into booking values(00215, '2017-02-10', 'N',439.0 , 'Y', 0, '0008f', 'N', 02046,'rodeoswiftsilver');
  insert into booking values(00216, '2017-01-14', 'N',490.0 , 'Y', 0, '0007f', 'N', 02046,'galliumflogging');
  insert into booking values(00217, '2017-01-14', 'N',100.30 , 'Y', 0, '0008f', 'N', 02046,'dipaxes');
  insert into booking values(00218, '2017-01-13', 'N',40.50 , 'Y', 0, '0006f', 'N', 02046,'dipaxes');
  insert into booking values(00219, '2017-02-14', 'N',42.20 , 'Y', 0, '0005f', 'Y', 02046,'alkynefreeze');
  
  insert into booking values(00220, '2017-02-14', 'N',19.70 , 'N', 0, '0005f', 'N', 03020,'jijisolutions');
  insert into booking values(00221, '2017-02-12', 'N',153.40 , 'N', 0, '0004f', 'Y', 03020,'playgirlannapolis');
  insert into booking values(00222, '2017-03-14', 'N',137.85 , 'N', 0, '0003f', 'N', 03020,'rawslight');
  insert into booking values(00223, '2017-02-05', 'N',330.50 , 'N', 0, '0003f', 'N', 03020,'rawslight');
  insert into booking values(00224, '2017-02-05', 'N',335.50 , 'N', 0, '0002f', 'Y', 03020,'potatoclutching');
  