use airline;

#past flights
insert into flight values(01001, 000101, 000102, '2016-04-01 17:20:00', '2016-04-01 20:30:00', 200, 'Terminal 1', 'arrived');
insert into flight values(01005, 000122, 000202, '2016-04-10 16:15:00', '2016-04-10 20:30:00', 220, 'Terminal 1', 'arrived');
insert into flight values(01007, 000211, 000311, '2016-04-02 10:00:00', '2016-04-02 15:00:00', 200, 'North Satellite Terminal', 'arrived');
insert into flight values(01010, 000202, 000511, '2016-04-13 06:45:00', '2016-04-13 17:30:00', 100, 'Domestic Terminal', 'arrived');
insert into flight values(01012, 000601, 000611, '2016-04-02 07:15:00', '2016-04-02 08:25:00', 200, 'Terminal 3', 'arrived');
insert into flight values(01037, 0006031, 000811, '2016-04-13 20:15:00', '2016-04-13 22:30:00', 300, 'Terminal 1', 'arrived');
insert into flight values(02001, 000611, 000831, '2016-04-17 16:00:00', '2016-04-18 06:00:00', 300, 'Terminal 1', 'arrived');
insert into flight values(02002, 0006021, 0008511, '2016-04-13 12:25:00', '2016-04-13 15:20:00', 200, 'Terminal 1', 'arrived');
insert into flight values(02005, 0008542, 0008541, '2016-04-15 12:00:00', '2016-04-15 20:00:00', 220, 'Terminal C', 'arrived');

insert into flight values(01033, 000801, 000832, '2017-04-13 06:15:00', '2017-04-13 10:30:00', 200, 'Terminal B', 'arrived');
insert into flight values(01032, 000831, 000832, '2017-04-13 17:45:00', '2017-04-13 22:30:00', 250, 'Terminal 1', 'arrived');
insert into flight values(01031, 0006041, 000701, '2017-04-19 06:15:00', '2017-04-19 10:30:00', 120, 'Terminal 1', 'arrived');
insert into flight values(01038, 0006031, 000811, '2017-04-13 20:15:00', '2017-04-13 22:30:00', 300, 'Terminal 1', 'arrived');
insert into flight values(02007, 000611, 000831, '2017-04-17 16:00:00', '2017-04-18 06:00:00', 300, 'Terminal 1', 'arrived');
insert into flight values(02008, 0006021, 0008511, '2017-04-13 12:25:00', '2017-04-13 15:20:00', 200, 'Terminal 1', 'arrived');
insert into flight values(02009, 0008542, 0008541, '2017-04-15 12:00:00', '2017-04-15 20:00:00', 220, 'Terminal C', 'arrived');

insert into flight values(02003, 0008541, 0008521, '2017-03-10 12:45:00', '2017-03-10 13:55:00', 200, 'Terminal 1', 'arrived');
insert into flight values(02109, 0008531, 0008521, '2017-03-10 16:15:00', '2017-03-10 20:30:00', 200, 'Terminal 1', 'arrived');
insert into flight values(02019, 0006021, 00085121, '2017-03-11 10:15:00', '2017-03-11 13:15:00', 100, 'Terminal 2', 'arrived');
insert into flight values(02020, 0008541, 0008543, '2017-03-10 20:15:00', '2017-03-11 00:30:00', 200, 'Terminal 1', 'arrived');
insert into flight values(02022, 00085121, 0008523, '2017-03-19 22:45:00', '2017-03-20 02:30:00', 250, 'Terminal 1', 'arrived');
insert into flight values(02025, 0006021, 0008523, '2017-03-02 03:15:00', '2017-03-02 06:30:00', 250, 'Terminal 2', 'arrived');
insert into flight values(02027, 00085121, 0008543, '2017-03-03 02:15:00', '2017-03-03 05:30:00', 100, 'Terminal 1', 'arrived');

insert into flight values(02030, 0008531, 0008521, '2017-02-01 20:15:00', '2017-02-01 11:55:00', 220, 'Terminal 2', 'arrived');
insert into flight values(02033, 0008533, 0008501, '2017-02-01 21:00:00', '2017-02-01 23:30:00', 210, 'Terminal 1', 'arrived');
insert into flight values(02034, 0008533, 0008531, '2017-02-02 17:15:00', '2017-02-02 21:30:00', 100, 'Terminal 1', 'arrived');
insert into flight values(02035, 0008543, 0008501, '2017-02-01 17:25:00', '2017-02-01 19:30:00', 300, 'Terminal C', 'arrived');
insert into flight values(02040, 0008521, 000841, '2017-02-05 23:35:00', '2017-02-06 02:30:00', 270, 'Terminal 1', 'arrived');
insert into flight values(02041, 0008521, 000831, '2017-02-01 20:15:00', '2017-02-01 22:30:00', 200, 'Terminal 2', 'arrived');
insert into flight values(02045, 0008521, 0006041, '2017-02-01 21:45:00', '2017-02-02 00:30:00', 200, 'Terminal 3', 'arrived');
insert into flight values(02046, 0008521, 0006031, '2017-02-10 17:15:00', '2017-02-10 21:30:00', 200, 'Terminal 4', 'arrived');

#future flights
insert into flight values(03020, 000801, 000811, '2017-05-15 10:10:00', '2017-05-15 20:10:00', 300, 'Terminal 1', 'on time');
insert into flight values(03022, 000121, 000102, '2017-05-17 19:20:00', '2017-05-17 21:35:00', 318, 'International Terminal', 'on time');
insert into flight values(03029, 000123, 000124, '2017-05-17 20:15:00', '2017-05-17 22:30:00', 500, 'Terminal 1', 'on time');
insert into flight values(03030, 000201, 000601, '2017-05-18 11:15:00', '2017-05-18 14:20:00', 200, 'Main Terminal', 'on time');
insert into flight values(03031, 000201, 000611, '2017-05-19 16:30:00', '2017-05-19 20:30:00', 220, 'Main Terminal', 'on time');
insert into flight values(03032, 000201, 0006031, '2017-05-20 10:10:00', '2017-05-20 15:10:00', 208, 'Main Terminal', 'on time');
insert into flight values(03035, 000201, 0006041, '2017-05-21 11:45:00', '2017-05-21 14:30:00', 220, 'Main Terminal', 'on time');

insert into flight values(03101, 000101, 000102, '2017-06-01 17:20:00', '2017-06-01 20:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(03105, 000122, 000202, '2017-06-10 16:15:00', '2017-06-10 20:30:00', 220, 'Terminal 1', 'on time');
insert into flight values(03107, 000211, 000311, '2017-06-10 10:00:00', '2017-06-10 15:00:00', 200, 'North Satellite Terminal', 'on time');
insert into flight values(03110, 000202, 000511, '2017-06-11 06:45:00', '2017-06-11 17:30:00', 100, 'Domestic Terminal', 'on time');
insert into flight values(03112, 000601, 000611, '2017-06-02 07:15:00', '2017-06-02 08:25:00', 200, 'Terminal 3', 'on time');
insert into flight values(03120, 000611, 001301, '2017-06-12 17:20:00', '2017-06-12 20:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(03124, 0006011, 0006021, '2017-06-11 17:20:00', '2017-06-11 20:30:00', 270, 'Seasonal Flight Terminal', 'on time');
insert into flight values(03123, 0007031, 000721, '2017-06-11 16:30:00', '2017-06-11 20:00:00', 200, 'Terminal 1', 'on time');
insert into flight values(03129, 0008601, 000831, '2017-06-12 08:15:00', '2017-06-12 20:30:00',  50, 'Terminal 4', 'on time'); #vip
insert into flight values(03130, 000801, 000811, '2017-06-11 12:15:00', '2017-06-11 13:30:00', 200, 'Terminal 1', 'on time');

insert into flight values(041001, 0008551, 0008541, '2017-07-01 16:30:00', '2017-07-01 20:45:00', 200, 'Terminal 1', 'on time');
insert into flight values(041011, 0008561, 0008532, '2017-07-02 06:45:00', '2017-07-02 08:10:00', 200, 'Terminal 2', 'on time');
insert into flight values(041021, 0008581, 0008532, '2017-07-03 10:10:00', '2017-07-03 12:40:00', 200, 'Terminal 1A', 'on time');
insert into flight values(041051, 0008571, 0008543, '2017-07-05 17:15:00', '2017-07-05 21:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(041101, 0008601, 000721, '2017-07-05 15:15:00', '2017-07-05 18:15:00', 200, 'Terminal 3', 'on time');
insert into flight values(040201, 00086011, 000841, '2017-07-06 18:45:00', '2017-07-06 21:15:00', 200, 'Terminal 2', 'on time');
insert into flight values(040251, 00086011, 0008621, '2017-07-10 21:15:00', '2017-07-10 22:10:00', 200, 'Terminal 1', 'on time'); #through amsterdam
insert into flight values(043041, 00086011, 0008621, '2017-07-11 16:30:00', '2017-07-11 20:15:00', 200, 'Terminal 2', 'on time'); #through amsterdam
insert into flight values(040211, 0008601, 0008681, '2017-07-06 18:45:00', '2017-07-06 21:15:00', 20, 'Terminal 4', 'on time'); #vip

insert into flight values(04100, 0008521, 0008541, '2017-07-01 16:30:00', '2017-07-01 20:45:00', 200, 'Terminal 1', 'on time');
insert into flight values(04101, 0008521, 0008532, '2017-07-02 06:45:00', '2017-07-02 08:10:00', 200, 'Terminal 2', 'on time');
insert into flight values(04102, 0008521, 0008532, '2017-07-03 10:10:00', '2017-07-03 12:40:00', 200, 'Terminal 3', 'on time');
insert into flight values(04105, 0008521, 0008543, '2017-07-05 17:15:00', '2017-07-05 21:30:00', 200, 'Terminal 4', 'on time');
insert into flight values(04110, 0008521, 000721, '2017-07-05 15:15:00', '2017-07-05 18:15:00', 200, 'Terminal 5', 'on time');
insert into flight values(04020, 0008521, 000841, '2017-07-06 18:45:00', '2017-07-06 21:15:00', 200, 'Terminal 1', 'on time');
insert into flight values(04025, 000841, 000831, '2017-07-10 21:15:00', '2017-07-10 22:10:00', 200, 'Terminal B', 'on time');
insert into flight values(04304, 000841, 000801, '2017-07-11 16:30:00', '2017-07-11 20:15:00', 200, 'Terminal B', 'on time');
insert into flight values(04305, 000841, 0007031, '2017-07-15 09:10:00', '2017-07-15 10:30:00', 200, 'Terminal D', 'on time');
insert into flight values(04331, 000841, 0006021, '2017-07-19 10:10:00', '2017-07-19 12:30:00', 200, 'Terminal D', 'on time');
insert into flight values(04351, 0006031, 0008511, '2017-07-20 17:10:00', '2017-07-20 19:35:00', 200, 'Terminal 1', 'on time');
insert into flight values(04377, 0008533, 000811, '2017-07-22 19:10:00', '2017-07-22 21:40:00', 200, 'Terminal 1', 'on time');
insert into flight values(04378, 000811, 000621, '2017-07-25 21:40:00', '2017-07-26 00:50:00', 200, 'South Terminal', 'on time');
insert into flight values(04400, 000811, 0008501, '2017-07-27 22:50:00', '2017-07-28 03:30:00', 200, 'South Terminal', 'on time');
insert into flight values(04444, 0006041, 0006011, '2017-07-29 23:15:00', '2017-07-29 05:35:00', 200, 'Terminal 2', 'on time');

insert into flight values(14100, 0008611, 0008631, '2017-07-01 16:30:00', '2017-07-01 20:45:00', 200, 'Terminal 1', 'on time');
insert into flight values(14101, 0008611, 0008631, '2017-07-01 06:45:00', '2017-07-01 08:10:00', 200, 'Terminal 1', 'on time');
insert into flight values(14102, 0008631, 0008641, '2017-07-01 10:10:00', '2017-07-01 12:40:00', 200, 'Terminal A', 'on time');
insert into flight values(14105, 0008651, 0008691, '2017-07-01 17:15:00', '2017-07-01 21:30:00', 200, 'Terminal 2', 'on time');
insert into flight values(14110, 0008681, 00086011, '2017-07-01 15:15:00', '2017-07-01 18:15:00', 200, 'Terminal 2', 'on time');
insert into flight values(14020, 0008701, 0008651, '2017-07-06 18:45:00', '2017-07-06 21:15:00', 200, 'Terminal 1', 'on time');
insert into flight values(14025, 0008681, 000901, '2017-07-10 21:15:00', '2017-07-10 22:10:00', 200, 'Terminal 1', 'on time');
insert into flight values(14304, 0008701, 0008711, '2017-07-11 16:30:00', '2017-07-11 20:15:00', 200, 'Terminal 2', 'on time');
insert into flight values(14305, 0009012, 000901, '2017-07-15 09:10:00', '2017-07-15 10:30:00', 200, 'Terminal A', 'on time');
insert into flight values(14331, 0008691, 0008651, '2017-07-19 10:10:00', '2017-07-19 12:30:00', 200, 'Terminal 3', 'on time');
insert into flight values(14351, 0008691, 0008691, '2017-07-20 17:10:00', '2017-07-20 19:35:00', 200, 'Terminal 1', 'on time');
insert into flight values(14377, 0008701, 0009012, '2017-07-22 19:10:00', '2017-07-22 21:40:00', 200, 'Terminal 1', 'on time');
insert into flight values(14378, 000901, 0008641, '2017-07-25 21:40:00', '2017-07-26 00:50:00', 200, 'Terminal 1', 'on time');
insert into flight values(14400, 000901, 0009012, '2017-07-27 22:50:00', '2017-07-28 03:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(14444, 000901, 00086011, '2017-07-29 23:15:00', '2017-07-29 05:35:00', 200, 'Terminal 1', 'on time');

insert into flight values(13101, 0009021, 0008661, '2018-06-10 17:20:00', '2018-06-10 20:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(13105, 001301, 0008501, '2018-06-10 16:15:00', '2018-06-10 20:30:00', 220, 'Infield Terminal', 'on time');
insert into flight values(13107, 001301, 0008661, '2018-06-10 10:00:00', '2018-06-10 15:00:00', 200, 'Terminal 1', 'on time');
insert into flight values(13110, 001101, 0008501, '2018-06-10 06:45:00', '2018-06-10 17:30:00', 100, 'Terminal 1', 'on time');
insert into flight values(13112, 001001, 001001, '2018-06-02 07:15:00', '2018-06-02 08:25:00', 200, 'Terminal A', 'on time');
insert into flight values(13120, 0009021, 0009012, '2018-06-12 17:20:00', '2018-06-12 20:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(13124, 001101, 0008701, '2018-06-11 17:20:00', '2018-06-11 20:30:00', 270, 'International Terminal', 'on time');
insert into flight values(13123, 001301, 001001, '2018-06-11 16:30:00', '2018-06-11 20:00:00', 200, 'Terminal 1', 'on time');
insert into flight values(13129, 001001, 001001, '2018-06-12 08:15:00', '2018-06-12 20:30:00',  50, 'Terminal B', 'on time'); 
insert into flight values(13130, 001002, 0009031, '2018-06-11 12:15:00', '2018-06-11 13:30:00', 200, 'Terminal 8', 'on time');

insert into flight values(05001, 000101, 001003, '2018-05-01 10:15:00', '2018-05-01 13:15:00', 250, 'Terminal 1', 'on time');
insert into flight values(05005, 000122, 000202, '2018-07-02 16:15:00', '2018-07-02 20:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(05010, 000211, 000311, '2018-08-02 20:15:00', '2018-08-02 23:30:00', 270, 'North Satellite Terminal', 'on time');
insert into flight values(05011, 000202, 000511, '2018-08-03 22:15:00', '2018-08-04 03:45:00', 100, 'Domestic Terminal', 'on time');
insert into flight values(05012, 000601, 000611, '2018-05-04 12:15:00', '2018-05-04 14:30:00', 200, 'Terminal 3', 'on time');
insert into flight values(05014, 000611, 001101, '2018-01-10 16:00:00', '2018-01-10 17:00:00', 220, 'Terminal 1', 'on time');
insert into flight values(05015, 0006011, 0006021, '2018-12-11 17:15:00', '2018-12-11 20:35:00', 250, 'Seasonal Flight Terminal', 'on time');
insert into flight values(05017, 0007031, 000721, '2018-11-11 10:20:00', '2018-1-11 12:30:00', 100, 'Terminal 1', 'on time');
insert into flight values(05018, 000521, 001201, '2018-10-14 09:30:00', '2018-10-14 10:45:00',  50, 'Terminal 2', 'on time');

insert into flight values(24025, 001301, 000321, '2018-07-11 21:15:00', '2018-07-11 22:10:00', 200, 'Terminal 1', 'on time');
insert into flight values(24304, 0008411, 00085311, '2018-07-11 16:30:00', '2018-07-11 20:15:00', 200, 'Terminal 1', 'on time');
insert into flight values(24305, 000331, 0002111, '2018-07-11 09:10:00', '2018-07-11 10:30:00', 200, 'West Wing', 'on time');
insert into flight values(24331, 000321, 000331, '2018-07-11 10:10:00', '2018-07-11 12:30:00', 200, 'Terminal 3', 'on time');
insert into flight values(24351, 00085311, 0008411, '2018-07-15 17:10:00', '2018-07-15 19:35:00', 200, 'Terminal 5', 'on time');
insert into flight values(24377, 00085311, 0008721, '2018-07-15 19:10:00', '2018-07-15 21:40:00', 200, 'Terminal 3', 'on time');
insert into flight values(24378, 001301, 0006051, '2018-07-15 21:40:00', '2018-07-16 00:50:00', 200, 'Terminal 3', 'on time');
insert into flight values(24400, 0002111, 00060111, '2018-07-15 22:50:00', '2018-07-16 03:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(24444, 0002111, 0008421, '2018-07-15 23:15:00', '2018-07-16 05:35:00', 200, 'Terminal 2', 'on time');

insert into flight values(25025, 00088221, 00087011, '2018-07-16 21:15:00', '2018-07-16 22:10:00', 200, 'Main Terminal', 'on time');
insert into flight values(25304, 000854011, 000741, '2018-07-16 16:30:00', '2018-07-16 20:15:00', 200, 'Terminal 1', 'on time');
insert into flight values(25305, 001401, 0008811, '2018-07-16 09:10:00', '2018-07-16 10:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(25331, 001401, 000801, '2018-07-16 10:10:00', '2018-07-16 12:30:00', 200, 'Terminal 2', 'on time');
insert into flight values(25351, 001401, 00088221, '2018-07-16 17:10:00', '2018-07-16 19:35:00', 200, 'Terminal 1', 'on time');
insert into flight values(25377, 0007211, 001401, '2018-07-19 19:10:00', '2018-07-19 21:40:00', 200, 'Terminal 2', 'on time');
insert into flight values(25378, 000751, 0007211, '2018-07-19 21:40:00', '2018-07-20 00:50:00', 200, 'Terminal 1', 'on time');
insert into flight values(25400, 000751, 000741, '2018-07-19 22:50:00', '2018-07-20 03:30:00', 200, 'Terminal 1', 'on time');
insert into flight values(25444, 0008821, 000751, '2018-07-19 23:15:00', '2018-07-20 05:35:00', 200, 'Satellite Terminal', 'on time');



