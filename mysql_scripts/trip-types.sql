use airline;
        
   delimiter $
   create trigger check_triptype before insert on `Trip Type`
	for each row 
		begin 
			if new.seaside not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'seaside : not Y or N';
			end if;
            if new.cultural not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'cultural : not Y or N';
			end if;
            if new.sport not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'sport : not Y or N';
			end if;
            if new.golf not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'golf : not Y or N';
			end if;
            if new.adventure not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'adventure : not Y or N';
			end if;
            if new.outdoor not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'outdoor : not Y or N';
			end if;
            if new.nightlife not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'nightlife : not Y or N';
			end if;
            if new.family not in ('Y','N') then
              signal sqlstate '45000' set message_text = 'family : not Y or N';
			end if;
	    end$
        delimiter ;
 
  #insert into `Trip Type` values(00011, 't', 'N', 'N', 'Y', 'Y', 'N', 'N', 'Y');
  #insert into `Trip Type` values(00012, 'N', 'r', 'Y', 'N', 'Y', 'N', 'N', 'N');
  #insert into `Trip Type` values(00011, 'N', 'N', 'e', 'Y', 'Y', 'N', 'N', 'Y');
  #insert into `Trip Type` values(00012, 'N', 'N', 'Y', 'w', 'Y', 'N', 'N', 'N');
  #insert into `Trip Type` values(00011, 'N', 'N', 'N', 'Y', 'q', 'N', 'N', 'Y');
  #insert into `Trip Type` values(00012, 'N', 'N', 'Y', 'N', 'Y', 'q', 'N', 'N');
  #insert into `Trip Type` values(00011, 'N', 'N', 'N', 'Y', 'Y', 'N', 's', 'Y');
  #insert into `Trip Type` values(00012, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'd');
 

  insert into `Trip Type` values(00010, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00011, 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00012, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00020, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00021, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000211, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00030, 'Y', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00031, 'Y', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00032, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
  insert into `Trip Type` values(00033, 'Y', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'N');
  
  insert into `Trip Type` values(00040, 'Y', 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(00041, 'Y', 'Y', 'N', 'N', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(00050, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(00051, 'Y', 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(00052, 'Y', 'Y', 'Y', 'N', 'Y', 'Y', 'N', 'Y');
  
  insert into `Trip Type` values(00060, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00061, 'Y', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00062, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000601, 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(0006011, 'Y', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000602, 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000603, 'N', 'Y', 'N', 'Y', 'N', 'N', 'N', 'Y');
  insert into `Trip Type` values(000604, 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N');
  insert into `Trip Type` values(000605, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'N');

  insert into `Trip Type` values(00070, 'N', 'N', 'N', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00071, 'Y', 'N', 'N', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00072, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000721, 'Y', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000703, 'Y', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00074, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00075, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  
  insert into `Trip Type` values(00080, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00081, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00082, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00083, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00084, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000842, 'Y', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000841, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000850, 'N', 'N', 'Y', 'N', 'Y', 'Y', 'N', 'Y');
  insert into `Trip Type` values(000851, 'N', 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(0008512, 'N', 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N');
  insert into `Trip Type` values(000852, 'N', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'Y');
  insert into `Trip Type` values(000853, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(0008531, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000854, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000855, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000856, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(00085401, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000857, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000858, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000859, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000860, 'N', 'Y', 'N', 'Y', 'N', 'N', 'N', 'Y');
  insert into `Trip Type` values(0008601, 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000861, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000862, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  
  insert into `Trip Type` values(000863, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000864, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000865, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000866, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000867, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000868, 'N', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'Y');
  
  insert into `Trip Type` values(000869, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000870, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000871, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'Y', 'Y');
  insert into `Trip Type` values(000872, 'N', 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(0008701, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000881, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000882, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(0008822, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  
  insert into `Trip Type` values(00090, 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000901, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(000902, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'Y');
  insert into `Trip Type` values(000903, 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  
  insert into `Trip Type` values(00100, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00110, 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00120, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00130, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N');
  insert into `Trip Type` values(00140, 'Y', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N');
  
  