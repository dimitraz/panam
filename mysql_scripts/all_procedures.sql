
#drop procedure get_all_bookings;

DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_bookings`(
  in username varchar(20)
)
BEGIN
  SELECT * FROM customer_bookings where Customer_username = username
  order by departing ASC;
END$

DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_in`(
  in cust_username varchar(20), book_id int
)
BEGIN
  update booking
  set checked_in = 'Y'
  where Customer_username = cust_username 
  and booking_id = book_id;
END$

# Procedures for home page, search for future flights
# to make a booking

#drop procedure find_flights_to_dest;
#drop procedure find_flights_from_dest_to_dest;

# Get flights from dest
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_flights_from_dest`(
  in city varchar(50)
)
BEGIN
  SELECT * FROM `all_flights` where `from` = city
  order by departing ASC;
END$

# Get flights to dest
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_flights_to_dest`(
  in city varchar(50)
)
BEGIN
  SELECT * FROM `all_flights` where `to` = city
  order by departing ASC;
END$
    
# Get flights from dest to dest
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_flights_from_dest_to_dest`(
  in fromcity varchar(50), in tocity varchar(50)
)
BEGIN
  SELECT * FROM `all_flights` where `from` = fromcity and `to` = tocity
  order by departing ASC;
END$


# Get flight by id
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_flight_by_id`(
  in flight_id int(11)
)
BEGIN
  SELECT * FROM `all_flights` where `id` = flight_id;
END$

# Procedures for home page, search for future flights
# to make a booking

#drop procedure get_flights_from_dest;
#drop procedure get_flights_from_dest_to_dest;
#drop procedure get_flights_to_dest;
#drop procedure get_flight_by_id;

# Get flights from dest 
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_flights_from_dest`(
  in city varchar(50)
)
BEGIN
  SELECT * FROM `future_flights` where `from` = city
  order by departing ASC;
END$
    
# Get flights from dest to dest
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_flights_from_dest_to_dest`(
  in fromcity varchar(50), in tocity varchar(50)
)
BEGIN
  SELECT * FROM `future_flights` where `from` = fromcity and `to` = tocity
  order by departing ASC;
END$

# Get flights to dest
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_flights_to_dest`(
  in city varchar(50)
)
BEGIN
  SELECT * FROM `future_flights` where `to` = city
  order by departing ASC;
END$

#drop procedure book_flight;
DELIMITER $
CREATE DEFINER=`root`@`localhost` PROCEDURE `book_flight`(
  in flight_id int(11), in username varchar(50)
)
BEGIN
  select @booking_id := max(booking_id) + 1 from booking;
  insert into booking values(@booking_id, sysdate(), 'N', 127.0 , 'N', 0, '0026d', 'N', flight_id , username);
  
  SELECT * FROM customer_bookings where Customer_username = username
  order by departing ASC;
END$
