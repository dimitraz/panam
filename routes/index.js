var db = require('../db');
var connection = db();
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    // connection.query('SELECT * FROM future_flights', function(err, rows){
    res.render('index');
    // });
});

router.post('/', function (req, res) {
    var request = "";
    if (req.body.todestination == '') {
        request = "call get_flights_from_dest('" + req.body.fromdestination + "')";
    } else if (req.body.fromdestination == '') {
        request = "call get_flights_to_dest('" + req.body.todestination + "')";
    } else {
        request = "call get_flights_from_dest_to_dest('" + req.body.fromdestination + "','" + req.body.todestination + "')";
    }
    console.log(request);

    connection.query(request, function (err, rows) {
        res.render('book', { flights: rows });
    });
});

module.exports = router;
