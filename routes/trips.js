var db = require('../db');
var connection = db();
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.render('trips');
});

// Seaside
router.get('/seaside', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `seaside` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

// Adventure
router.get('/adventure', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `adventure` = "Y"', function (err, rows) {
        console.log(rows)
        res.render('trip', { trips: rows });
    });
});

// Culturual
router.get('/culture', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `cultural` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

// Nightlife
router.get('/nightlife', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `nightlife` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

// Outdoor
router.get('/outdoor', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `outdoor` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

// Golf
router.get('/golf', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `golf` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

// Family
router.get('/family', function (req, res) {
    connection.query('SELECT * FROM `Trip Type`, `Destination` WHERE fk_destination_id = destination_id AND `family` = "Y"', function (err, rows) {
        res.render('trip', { trips: rows });
    });
});

/*
router.post('/', function(req, res){
  var request = "";
  if(req.body.todestination == '') {
    request = "call get_flights_from_dest('" + req.body.fromdestination + "')";
  } else if (req.body.fromdestination == '') {
    request = "call get_flights_to_dest('" + req.body.todestination + "')";
  } else {
    request = "call get_flights_from_dest_to_dest('" + req.body.fromdestination + "','"+ req.body.todestination+"')";
  }
  console.log(request);
  connection.query(request, function(err, rows){
    res.render('index', { get_flights_to_dest: rows });
  });
});
*/

module.exports = router;
