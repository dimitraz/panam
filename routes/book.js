var db = require('../db');
var connection = db();
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.render('book');
});

router.post('/', function (req, res) {
    var request = "";
    if (req.body.todestination == '') {
      request = "call get_flights_from_dest('" + req.body.fromdestination + "')";
    } else if (req.body.fromdestination == '') {
        request = "call get_flights_to_dest('" + req.body.todestination + "')";
    } else {
      if(typeof req.body.username == 'undefined') {
        request = "call get_flights_from_dest_to_dest('" + req.body.fromdestination + "','" + req.body.todestination + "')";
      } else {
        request = "call book_flight(" + req.body.flight_id + ",\'" + req.body.username + "\')";
      }
    }
    console.log(request);
      connection.query(request, function (err, rows) {
        if(typeof req.body.username == 'undefined') {
          res.render('book', { flights: rows });
        } else {
          console.log(rows[1]);
          res.render('bookings', { all_bookings: rows[1] });
        }
      });
  });

module.exports = router;
