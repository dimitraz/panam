var db = require('../db');
var connection = db();
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    // connection.query('SELECT * FROM all_flights', function (err, rows) {
    res.render('find');
    // });
});

router.post('/', function (req, res) {
    var request = "";
    if (typeof req.body.flight_id == 'undefined') {
        if (req.body.todestination == '') {
            request = "call find_flights_from_dest('" + req.body.fromdestination + "')";
        } else if (req.body.fromdestination == '') {
            request = "call find_flights_to_dest('" + req.body.todestination + "')";
        } else {
            request = "call find_flights_from_dest_to_dest('" + req.body.fromdestination + "','" + req.body.todestination + "')";
        }
    }
    else {
        request = "call find_flight_by_id(" + req.body.flight_id + ")";
    }
    console.log("Flight id: " + req.body.flight_id + " From: " + req.body.fromdestination + " To: " + req.body.todestination);
    console.log(request);
    connection.query(request, function (err, rows) {
        console.log(rows);
        res.render('find', { flights: rows });
    });
});

module.exports = router;
