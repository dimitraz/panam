var db = require('../db');
var connection = db();
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    connection.query('call get_all_bookings(\'\')', function (err, rows) {
        res.render('bookings', { all_bookings: rows[0] });
    });
});

router.post('/', function (req, res) {
    var request = "call get_all_bookings(\'" + req.body.username + "\')";
    var check_in = "";
    if(req.body.booking_id != 'undefined') {
      check_in = "call check_in(\'" + req.body.username + "\'," + req.body.Booking_id + ")";
      connection.query(check_in, function (req, rows) {});
      console.log(check_in);
    }
    console.log(request);
    connection.query(request, function (err, rows) {
        res.render('bookings', { all_bookings: rows[0] });
    });
});

module.exports = router;
