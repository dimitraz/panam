var express = require('express');
var hbs = require('hbs');
var mysql = require('mysql');
var path = require('path');
var bodyParser = require('body-parser');

// routes
var index = require('./routes/index.js');
var find = require('./routes/find.js');
var bookings = require('./routes/bookings.js');
var trips = require('./routes/trips.js');
var book = require('./routes/book.js');

var app = express()

// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'hbs');
app.set('port', (process.env.PORT || 5000));
app.use(express.static(path.join(__dirname, '/public')));


// get inputs from forms using req.body.variable_name
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', index);
app.use('/find', find);
app.use('/trips', trips);
app.use('/bookings', bookings);
app.use('/book', book);

// connect to database
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "airline"
});

connection.connect(function(err) {
  if (err) throw err
  else console.log('You are now connected...')
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
